""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" Load plugins via vundle 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" set nocompatible              " be iMproved, required
" filetype off                  " required

"""" set the runtime path to include Vundle and initialize
"set rtp+=~/.vim/bundle/Vundle.vim
"call vundle#begin()
"Plugin 'VundleVim/Vundle.vim'

"""" Plugins 
"Plugin 'zxqfl/tabnine-vim'
"Plugin 'powerline/powerline'

"""" All of your Plugins must be added before the following line
"call vundle#end()            " required
"filetype plugin indent on    " required


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Spaces & Tabs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set tabstop=4 " number of visual spaces per TAB"
set softtabstop=4 " number of spaces in tab when editing
set smarttab
set expandtab

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" UI
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set number " show line numbers
set ruler
set cursorline

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Escape insert mode
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
:imap jk <Esc>
:imap kj <Esc>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Shift lines up and down
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" breaking habbits 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
