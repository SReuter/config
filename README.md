''' This project is for setting up and customization of a linux terminal environment '''

sudo apt-get update

sudo apt-get upgrade

sudo apt-get install --yes openssh-server vim screen zsh

git clone git@gitlab.com:SReuter/config.git

mv config .config

cd ~

ln -s .profile/.vimrc

ln -s .profile/.screenrc

### Configure vim ##################

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.environment

''' open .vimrc, uncomment Vundle and Plugins-Section

''' type ':PluginInstall' for installation of pliugins

### Configure ZSH ##################

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

sed -i -e 's/^ZSH_THEME=".*"$/ZSH_THEME="agnoster"/' ~/.zshrc

sudo apt-get install fonts-powerline 

